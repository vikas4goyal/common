# Common

[![CI Status](http://img.shields.io/travis/opensourcegit/Common.svg?style=flat)](https://travis-ci.org/opensourcegit/Common)
[![Version](https://img.shields.io/cocoapods/v/Common.svg?style=flat)](http://cocoapods.org/pods/Common)
[![License](https://img.shields.io/cocoapods/l/Common.svg?style=flat)](http://cocoapods.org/pods/Common)
[![Platform](https://img.shields.io/cocoapods/p/Common.svg?style=flat)](http://cocoapods.org/pods/Common)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Common is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Common"
```

## Author

opensourcegit, vikas.goyal@webvirtue.com

## License

Common is available under the MIT license. See the LICENSE file for more info.
